package szymon15.ru.is.thepuzzlegamedots.ranking;

import java.io.Serializable;

/**
 * Created by szymo on 10.09.2015.
 */
public class Ranking implements Serializable {
    private String m_name;
    private int m_cool;


    public Ranking(String name, int cool) {
        m_name = name;
        m_cool = cool;
    }

    public String getName() {
        return m_name;
    }

    public int getCool() {
        return m_cool;
    }

    @Override
    public String toString() {
        return m_name + " " + m_cool;
    }

}
