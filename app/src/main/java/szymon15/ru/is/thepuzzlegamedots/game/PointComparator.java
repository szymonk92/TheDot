package szymon15.ru.is.thepuzzlegamedots.game;

import android.graphics.Point;

import java.util.Comparator;

/**
 * Created by szymo on 13.09.2015.
 */
public class PointComparator implements Comparator<Point> {

    @Override
    public int compare(Point lhs, Point rhs) {
        return lhs.y - rhs.y;
    }
}