package szymon15.ru.is.thepuzzlegamedots.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import szymon15.ru.is.thepuzzlegamedots.MainActivity;
import szymon15.ru.is.thepuzzlegamedots.R;

public class UserName extends AppCompatActivity {

    public final static String USER_NAME = "user_name";
    public final static String SHARED_PREFERENCES = "szymon15.ru.is.thepuzzlegamedots";

    private Button btnOk;
    private EditText etUserName;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_name);

        btnOk = (Button) findViewById(R.id.btnOkUserName);
        etUserName = (EditText) findViewById(R.id.userName);

        prefs = this.getSharedPreferences(
                SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void addUsername(View view) {
        prefs.edit().putString(USER_NAME, String.valueOf(etUserName.getText())).apply();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
