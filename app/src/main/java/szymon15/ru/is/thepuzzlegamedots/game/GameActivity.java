package szymon15.ru.is.thepuzzlegamedots.game;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import szymon15.ru.is.thepuzzlegamedots.MainActivity;
import szymon15.ru.is.thepuzzlegamedots.R;
import szymon15.ru.is.thepuzzlegamedots.ranking.RankingActivity;

public class GameActivity extends AppCompatActivity {

    public static final String SCORES = "SCORES";
    public static final String ADD_SCORE = "AFTER_GAME";
    String gameType;
    private TextView tvScore;
    private TextView tvMoves;
    private BoardView m_bv;
    private int scoresA;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        m_bv = (BoardView) findViewById(R.id.boardView);

        tvScore = (TextView) findViewById(R.id.scores);
        tvMoves = (TextView) findViewById(R.id.moves);

        intent = getIntent();
        gameType = intent.getStringExtra(MainActivity.GAME_TYPE);

        int moves = gameType.equals(MainActivity.INFINITE) ? 99999 : 30;
        m_bv.moves = moves;
        setMoves(moves);

        m_bv.setMoveEventHandler(new OnMoveEventHandler() {
            @Override
            public void scoresUpdate(int scores) {
                scoresA = scores;
                tvScore.setText(String.valueOf(scoresA));
            }

            @Override
            public void moveUpdate(int moves) {
                setMoves(moves);
            }

            @Override
            public void newActivity() {
                Intent intent = new Intent(getApplicationContext(), RankingActivity.class);
                intent.putExtra(SCORES, scoresA);
                intent.putExtra(ADD_SCORE, true);
                startActivity(intent);
            }
        });
    }

    private void setMoves(int moves) {
        if (gameType.equals(MainActivity.INFINITE)) {
            tvMoves.setText("∞");
        } else {
            tvMoves.setText(String.valueOf(moves));
        }
    }

    //to change color of the window//
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        String background_chooser = getPrefs
                .getString("prefSetBackground", "1");
        View view = findViewById(R.id.layoutMain);

        if (background_chooser.equals("1")) {
            view.setBackgroundColor(Color.WHITE);
        } else if (background_chooser.equals("2")) {
            view.setBackgroundColor(Color.GREEN);
        } else if (background_chooser.equals("3")) {
            view.setBackgroundColor(Color.BLUE);
        } else {
            view.setBackgroundColor(Color.YELLOW);
        }
    }

    public void shuffle(View view) {
        m_bv.shuffle();
    }

}
