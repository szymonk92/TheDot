package szymon15.ru.is.thepuzzlegamedots.settings;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import szymon15.ru.is.thepuzzlegamedots.R;
import szymon15.ru.is.thepuzzlegamedots.ranking.RankingActivity;

/**
 * Created by szymo on 19.09.2015.
 */
public class GamePreferencesFragment extends PreferenceFragment {

    public static final String RESET = "RESET";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        Preference button = findPreference(getString(R.string.reset_button_preferences));
        button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(getActivity(), RankingActivity.class);
                intent.putExtra(RESET, true);
                startActivity(intent);
                return true;
            }
        });
    }
}
