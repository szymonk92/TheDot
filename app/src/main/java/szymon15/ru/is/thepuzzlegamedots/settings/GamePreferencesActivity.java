package szymon15.ru.is.thepuzzlegamedots.settings;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by szymo on 10.09.2015.
 */
public class GamePreferencesActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new GamePreferencesFragment())
                .commit();
    }
}
