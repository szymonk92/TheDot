package szymon15.ru.is.thepuzzlegamedots;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import szymon15.ru.is.thepuzzlegamedots.game.GameActivity;
import szymon15.ru.is.thepuzzlegamedots.ranking.RankingActivity;
import szymon15.ru.is.thepuzzlegamedots.settings.GamePreferencesActivity;
import szymon15.ru.is.thepuzzlegamedots.settings.UserName;

public class MainActivity extends AppCompatActivity {

    public static final String GAME_TYPE = "GAME_TYPE";
    public static final String IS_FIRST = "is_first";
    public static final String INFINITE = "INFINITE";
    public static final String THIRTY = "THIRTY";

    private static final String MY_PREFERENCES = "my_preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public boolean isFirst(Context context) {
        final SharedPreferences reader = context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        final boolean first = reader.getBoolean(IS_FIRST, true);
        if (first) {
            Intent intent = new Intent(this, UserName.class);
            startActivity(intent);
            final SharedPreferences.Editor editor = reader.edit();
            editor.putBoolean(IS_FIRST, false);
            editor.commit();
        }
        return first;
    }

    @Override
    protected void onStart() {
        super.onStart();
        isFirst(getApplicationContext());
    }

    public void play(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(GAME_TYPE, THIRTY);
        startActivity(intent);
    }

    public void statistic(View view) {
        Intent intent = new Intent(this, RankingActivity.class);
        startActivity(intent);
    }

    public void options(View view) {
        Intent intent = new Intent(this, GamePreferencesActivity.class);
        startActivity(intent);
    }


    public void some(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(GAME_TYPE, INFINITE);
        startActivity(intent);
    }


}
