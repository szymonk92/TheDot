package szymon15.ru.is.thepuzzlegamedots.game;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import szymon15.ru.is.thepuzzlegamedots.R;


/**
 * Created by szymo on 31.08.2015.
 */
public class BoardView extends View {


    private static final float DOT_PADDING_SIZE = 0.2f;
    private static final int VIBRATE_DURATION = 300;

    //Pair of dots and pain parameters
    public Pair dots[][];
    public int moves;
    private SharedPreferences sharedPreferences;
    //List of paints with colors
    private List<Paint> paints = new ArrayList<>();
    private List<Animator> animations = new ArrayList<Animator>();
    private int NUM_CELLS;  //get from preferences
    private int[] androidColors = getResources().getIntArray(R.array.dot_colors);
    private int numberOfColors = androidColors.length;
    private boolean m_moving = false;// tells when I move the circle
    private Rect m_rect = new Rect();
    private Paint m_paint = new Paint();
    private Path m_path = new Path();
    private Paint m_paintPath = new Paint();
    private int m_cellWidth;
    private int m_cellHeight;
    private List<Point> m_cellPath = new ArrayList<>();
    private OnMoveEventHandler m_moveHandler = null;
    private int scores = 0;
    private Boolean useVibrator;
    private Vibrator vibrator;

    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        NUM_CELLS = Integer.valueOf(sharedPreferences.getString(getContext().getString(R.string.pref_num_filds), "6"));
        dots = new Pair[NUM_CELLS][NUM_CELLS];
        useVibrator = sharedPreferences.getBoolean(getResources().getString(R.string.vibrate), false);
        vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);

        setPaintGrid();
        setPaintCircles();
        setPaintStroke();
    }

    private void setPaintCircles() {
        for (int i = 0; i < numberOfColors; i++) {
            Paint panCir = new Paint();
            panCir.setColor(androidColors[i]);
            panCir.setStyle(Paint.Style.FILL_AND_STROKE);
            panCir.setAntiAlias(true);
            paints.add(panCir);
        }
    }

    private void setPaintStroke() {
        m_paintPath.setColor(Color.BLACK);
        m_paintPath.setStrokeWidth(7);
        m_paintPath.setStyle(Paint.Style.STROKE);
        m_paintPath.setAntiAlias(true);
    }

    private void setPaintGrid() {
        m_paint.setColor(Color.BLACK);
        m_paint.setAlpha(0);
        m_paint.setStyle(Paint.Style.STROKE);
        m_paint.setStrokeWidth(2);
        m_paint.setAntiAlias(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        int height = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        int size = Math.min(width, height);
        setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(),
                size + getPaddingTop() + getPaddingBottom());
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        int boardWidth = (xNew - getPaddingLeft() - getPaddingRight());
        int boardHeight = (yNew - getPaddingTop() - getPaddingBottom());

        m_cellWidth = boardWidth / NUM_CELLS;
        m_cellHeight = boardHeight / NUM_CELLS;

        for (int i = 0; i < NUM_CELLS; i++) {
            for (int j = 0; j < NUM_CELLS; j++) {
                RectF cir = setCircle(i, j);
                dots[i][j] = (new Pair(cir, paints.get(new Random().nextInt(numberOfColors))));
            }
        }
    }

    private RectF setCircle(int x, int y) {
        RectF cir = new RectF();
        cir.set(m_cellWidth * x,
                m_cellHeight * y,
                m_cellWidth * (x + 1),
                m_cellHeight * (y + 1));

        cir.offset(getPaddingLeft(), getPaddingTop());
        cir.inset(m_cellWidth * DOT_PADDING_SIZE, m_cellHeight * DOT_PADDING_SIZE);
        return cir;
    }

    @Override
    protected void onDraw(Canvas canvas) {

        canvas.drawRect(m_rect, m_paint);
        for (int i = 0; i < NUM_CELLS; i++) {
            for (int j = 0; j < NUM_CELLS; j++) {
                int x = i * m_cellWidth;
                int y = j * m_cellHeight;
                m_rect.set(x, y, x + m_cellWidth, y + m_cellHeight);
                m_rect.offset(getPaddingLeft(), getPaddingTop());
                canvas.drawRect(m_rect, m_paint);
            }
        }

        if (!m_cellPath.isEmpty()) {
            m_path.reset();
            Point point = m_cellPath.get(0);
            m_path.moveTo(colToX(point.x) + m_cellWidth / 2, rowToY(point.y) + m_cellHeight / 2);
            for (int i = 1; i < m_cellPath.size(); i++) {
                point = m_cellPath.get(i);
                m_path.lineTo(colToX(point.x) + m_cellWidth / 2, rowToY(point.y) + m_cellHeight / 2);
            }
            canvas.drawPath(m_path, m_paintPath);
        }

        for (int i = 0; i < NUM_CELLS; i++) {
            for (int j = 0; j < NUM_CELLS; j++) {
                canvas.drawOval((RectF) dots[i][j].first, (Paint) dots[i][j].second);
            }
        }

    }

    private int xToCol(int x) {
        return (x - getPaddingLeft()) / m_cellWidth;
    }

    private int yToRow(int y) {
        return (y - getPaddingTop()) / m_cellHeight;
    }

    private int colToX(int col) {
        return col * m_cellWidth + getPaddingLeft();
    }

    private int rowToY(int row) {
        return row * m_cellHeight + getPaddingTop();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int xMax = getPaddingLeft() + m_cellWidth * NUM_CELLS;
        int yMax = getPaddingTop() + m_cellHeight * NUM_CELLS;

        int x = Math.max(getPaddingLeft(), Math.min((int) event.getX(),
                (int) (xMax - ((RectF) dots[0][0].first).width())));

        int y = Math.max(getPaddingTop(), Math.min((int) event.getY(),
                (int) (yMax - ((RectF) dots[0][0].first).height())));

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            m_moving = true;
            m_cellPath.add(new Point(xToCol(x), yToRow(y))); //Points: 1 2 3 4 5

        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            actionMove(x, y);

        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            actionUp();
        }
        return true;
    }

    private void actionUp() {
        if (useVibrator) {
            vibrator.vibrate(VIBRATE_DURATION);
        }

        if (m_cellPath.size() > 1) {
            m_moving = false;
            m_cellPath = removeDuplicatesAndSort(m_cellPath);
            setMovesAndScores(m_cellPath.size());

            moveDots();
            startAnimation();
        }
        m_cellPath.clear();
        invalidate();
    }

    private void actionMove(int x, int y) {
        if (m_moving) {
            if (!m_cellPath.isEmpty()) {
                int col = xToCol(x);
                int row = yToRow(y);
                Point last = m_cellPath.get(m_cellPath.size() - 1);
                connectDots(col, row, last);
            }
        }
    }

    private void connectDots(int col, int row, Point last) {
        //check if dots are neighbours
        if ((col != last.x || row != last.y)
                && Math.sqrt(Math.pow(col - last.x, 2) + Math.pow(row - last.y, 2)) <= 1) {

            if (((Paint) dots[last.x][last.y].second).getColor()
                    == ((Paint) dots[col][row].second).getColor()) {
                if (m_cellPath.size() > 1) {

                    if (!(m_cellPath.get(m_cellPath.size() - 2).equals(new Point(col, row)))) { // zapobiega wracaniu się
                        m_cellPath.add(new Point(col, row));
                    } else {
                        m_cellPath.remove(m_cellPath.size() - 1);
                    }
                } else {
                    m_cellPath.add(new Point(col, row));
                }
            }
            invalidate();
        }
    }

    public void shuffle() {
        for (int i = 0; i < NUM_CELLS; i++) {
            for (int j = 0; j < NUM_CELLS; j++) {
                dots[i][j] = new Pair(setCircle(i, j), paints.get(new Random().nextInt(numberOfColors)));
            }
        }
        invalidate();
    }

    private void moveDots() {
        for (int i = 0; i < m_cellPath.size(); i++) {
            int iks = m_cellPath.get(i).x;
            int igrek = m_cellPath.get(i).y;
            RectF cir = setCircle(iks, igrek);

            if (m_cellPath.get(i).y > 0) { // Check if it's top row
                dots[iks][igrek] = new Pair(cir, dots[iks][igrek - 1].second);
                m_cellPath.add(new Point(iks, igrek - 1)); // Add to the path

            } else {
                dots[iks][igrek] = new Pair(cir, paints.get(new Random().nextInt(numberOfColors)));
            }
            animations.add(animateMovement(cir, cir.left, cir.top - m_cellHeight, cir.left, cir.top));
        }
    }

    private void startAnimation() {
        Log.i("ANI", "Start param");
        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animations);
        animatorSet.start();
        animations.clear();
    }

    private void setMovesAndScores(int scoresNow) {
        moves--;
        scores += scoresNow;
        if (moves <= 0 && m_moveHandler != null) {
            m_moveHandler.scoresUpdate(scores);
            m_moveHandler.newActivity();
        }
        if (m_moveHandler != null) {
            m_moveHandler.scoresUpdate(scores);
            m_moveHandler.moveUpdate(moves);
        }

    }

    private List<Point> removeDuplicatesAndSort(List<Point> points) {

        List<Point> dotsWithDuplicates = points;

        Set<Point> hs = new HashSet<>();
        hs.addAll(dotsWithDuplicates);
        dotsWithDuplicates.clear();
        dotsWithDuplicates.addAll(hs);
        hs.clear();

        Collections.sort(dotsWithDuplicates, new PointComparator());//Sort by Y
        return m_cellPath;
    }


    private ValueAnimator animateMovement(final RectF circle, final float xFrom, final float yFrom,
                                          final float xTo, final float yTo) {

        final ValueAnimator valA = new ValueAnimator();
        valA.removeAllUpdateListeners();
        valA.setDuration(300);
        valA.setFloatValues(0.0f, 1.0f);
        valA.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float ratio = (float) valA.getAnimatedValue();
                int x = (int) ((1.0f - ratio) * xFrom + ratio * xTo);
                int y = (int) ((1.0f - ratio) * yFrom + ratio * yTo);
                Log.i("ANI", "y=" + y);
                circle.offsetTo(x, y);
                invalidate();
            }
        });
        return valA;

    }

    public void setMoveEventHandler(OnMoveEventHandler handler) {
        m_moveHandler = handler;
    }
}

//http://www.techrepublic.com/blog/software-engineer/behold-the-animation-magic-of-an-android-interpolator/
