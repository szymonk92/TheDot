package szymon15.ru.is.thepuzzlegamedots.ranking;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import szymon15.ru.is.thepuzzlegamedots.R;

/**
 * Created by szymo on 10.09.2015.
 */
public class RankingAdapter extends ArrayAdapter<Ranking> {

    private final Context context;
    private final List<Ranking> values;


    public RankingAdapter(Context context, List<Ranking> objects) {
        super(context, -1, objects);
        this.context = context;
        this.values = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_layout, parent, false);

        TextView nameView = (TextView) rowView.findViewById(R.id.row_name);
        nameView.setTextColor(ContextCompat.getColor(getContext(), R.color.purple));
        nameView.setText(values.get(position).getName());
        TextView scoresView = (TextView) rowView.findViewById(R.id.scores);
        scoresView.setText(String.valueOf(values.get(position).getCool()));
        return rowView;
    }
}