package szymon15.ru.is.thepuzzlegamedots.game;

/**
 * Created by szymo on 14.09.2015.
 */
public interface OnMoveEventHandler {

    void scoresUpdate(int scores);

    void moveUpdate(int moves);

    void newActivity();
}
