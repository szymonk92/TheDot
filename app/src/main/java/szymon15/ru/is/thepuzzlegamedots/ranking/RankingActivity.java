package szymon15.ru.is.thepuzzlegamedots.ranking;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import szymon15.ru.is.thepuzzlegamedots.MainActivity;
import szymon15.ru.is.thepuzzlegamedots.R;
import szymon15.ru.is.thepuzzlegamedots.game.GameActivity;
import szymon15.ru.is.thepuzzlegamedots.settings.GamePreferencesFragment;
import szymon15.ru.is.thepuzzlegamedots.settings.UserName;

/**
 * Created by szymo on 10.09.2015.
 */
public class RankingActivity extends AppCompatActivity {

    private static final String FILE_NAME = "rankingDots.ser";
    public static ArrayList<Ranking> m_data = new ArrayList<>();
    private RankingAdapter m_adapter;
    private Intent intent;
    private ListView m_listView;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rank_layout);

        m_listView = (ListView) findViewById(R.id.records);
        m_adapter = new RankingAdapter(this, m_data);
        m_listView.setAdapter(m_adapter);

        prefs = this.getSharedPreferences(
                UserName.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        intent = getIntent();

    }

    @Override
    public void onStart() {
        super.onStart();
        readRecords();
        m_adapter.notifyDataSetChanged();

        if (intent.getBooleanExtra(GameActivity.ADD_SCORE, false)) {
            storeRecord();
            intent.removeExtra(GameActivity.ADD_SCORE);
        }

        if (intent.getBooleanExtra(GamePreferencesFragment.RESET, false)) {
            m_data.clear();
            intent.removeExtra(GamePreferencesFragment.RESET);
            Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.ranking_clear), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        writeRecords();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void storeRecord() {
        String name = prefs.getString(UserName.USER_NAME, "hero");
        int scores = intent.getIntExtra(GameActivity.SCORES, 10);
        m_data.add(0, new Ranking(name, scores));
        m_adapter.notifyDataSetChanged();
    }

    void writeRecords() {
        try {
            FileOutputStream fos = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(m_data);
            oos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    void readRecords() {

        try {
            FileInputStream fis = openFileInput(FILE_NAME);
            ObjectInputStream oos = new ObjectInputStream(fis);
            ArrayList<Ranking> records = (ArrayList) oos.readObject();
            m_data.clear();
            for (Ranking record : records) {
                m_data.add(record);
            }
            oos.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

}
